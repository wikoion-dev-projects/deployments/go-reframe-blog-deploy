version: "3.3"

secrets:
  CF_API_EMAIL:
    file: "../secrets/CF_API_EMAIL.secret"
  CF_API_KEY:
    file: "../secrets/CF_API_KEY.secret"
  GO_API_KEY:
    file: "../secrets/GO_API_KEY.secret"

networks:
  traefik_proxy:
    external:
      name: traefik_proxy
  default:
    driver: bridge

services:
  traefik:
    image: "traefik:v2.2"
    container_name: "traefik"
    networks:
      - default
      - traefik_proxy
    command:
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.web.http.redirections.entryPoint.to=websecure"
      - "--entrypoints.web.http.redirections.entryPoint.scheme=https"
      - "--entrypoints.websecure.address=:443"
      - "--certificatesresolvers.myresolver.acme.dnschallenge=true"
      - "--certificatesresolvers.myresolver.acme.dnschallenge.provider=cloudflare"
      - "--certificatesresolvers.myresolver.acme.email=7a8kmtu2tvfv@opayq.com"
      - "--certificatesresolvers.myresolver.acme.storage=/letsencrypt/acme.json"
    labels:
      - "traefik.http.routers.api.tls.domains[0].main=fossg.news"
      - "traefik.http.routers.api.tls.domains[0].sans=*.fossg.news"
    ports:
      - "80:80"
      - "443:443"
      - "8081:8080"
    secrets:
      - "CF_API_EMAIL"
      - "CF_API_KEY"
    environment:
      - "CF_API_EMAIL_FILE=/run/secrets/CF_API_EMAIL"
      - "CF_API_KEY_FILE=/run/secrets/CF_API_KEY"
    volumes:
      - "../letsencrypt:/letsencrypt"
      - "/var/run/docker.sock:/var/run/docker.sock:ro"

  watchtower:
    command: --label-enable --cleanup --interval 300
    image: containrrr/watchtower
    labels:
      - "com.centurylinklabs.watchtower.enable=true"
    network_mode: none
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

  
  reframe-blog:
    image: 'registry.gitlab.com/wikoion-dev-projects/clojure-projects/reframe-blog'
    container_name: 'reframe-blog-app'
    networks:
      - traefik_proxy
    ports:
      - '3001:3000'
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.reframe.rule=Host(`cmj.fossg.news`)"
      - "traefik.http.routers.reframe.entrypoints=websecure"
      - "traefik.http.routers.reframe.tls.certresolver=myresolver"
      - "com.centurylinklabs.watchtower.enable=true"
    links:
      - golang-blog
    depends_on:
      - golang-blog

  golang-blog:
    image: 'registry.gitlab.com/wikoion-dev-projects/go-projects/golang-blog'
    container_name: 'golang-blog-app'
    networks:
      - traefik_proxy
    ports:
      - '8080:8080'
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.golang.rule=Host(`api.fossg.news`)"
      - "traefik.http.routers.golang.entrypoints=websecure"
      - "traefik.http.routers.golang.tls.certresolver=myresolver"
      - "com.centurylinklabs.watchtower.enable=true"
    secrets:
      - "GO_API_KEY"
    environment:
      - "API_KEY_FILE=/run/secrets/GO_API_KEY"
    links:
      - database
    depends_on:
      - database

  database-setup:
    container_name: database-setup
    image: mongo
    restart: on-failure
    networks:
      - traefik_proxy
    volumes:
      - ../mongodb/scripts:/scripts
    entrypoint: [ "/scripts/setup.sh" ]
    depends_on:
      - database

  database:
    image: 'mongo:4.2.5'
    container_name: 'golang-blog-db'
    networks:
      - traefik_proxy
    environment:
      - MONGO_INITDB_DATABASE=golang-blog-db
      - MONGO_INITDB_ROOT_USERNAME=madmin
      - MONGO_INITDB_ROOT_PASSWORD=madmin
    volumes:
      - ../mongodb/mongo-volume:/data/db
      - ../mongodb/scripts:/tmp/scripts
    ports:
      - '27017-27019:27017-27019'
    command: "--bind_ip_all --replSet rs0"

  elasticsearch:
    restart: always
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    networks:
      - traefik_proxy
    ports:
      - "9200:9200"
      - "9300:9300"
    volumes:
      - ../elastic:/usr/share/elasticsearch/data
    depends_on:
      - database
    environment:
      - ES_JAVA_OPTS=-Xms2048m -Xmx2048m
      - discovery.type=single-node
    healthcheck:
      test: "wget -q -O - http://localhost:9200/_cat/health"
      interval: 1s
      timeout: 30s
      retries: 300
    ulimits:
      memlock:
        soft: -1
        hard: -1

  monstache:
    restart: always
    image: 'registry.gitlab.com/container-images1/monstache-docker-image'
    networks:
      - traefik_proxy
    expose:
      - "8080"
    ports:
      - "8090:8080"
    depends_on:
      - database
      - elasticsearch
    environment:
      - MONSTACHE_MONGO_URL=mongodb://madmin:madmin@database:27017/?replicaSet=rs0
      - MONSTACHE_ES_URLS=http://elasticsearch:9200
    links:
      - elasticsearch
      - database
