#!/bin/bash

MONGODB1=database
# MONGODB2=mongo2
# MONGODB3=mongo3

echo "**********************************************" ${MONGODB1}
echo "Waiting for startup.."
until curl http://${MONGODB1}:27017/serverStatus\?text\=1 2>&1 | grep uptime | head -1; do
  printf '.'
  sleep 1
done

echo SETUP.sh time now: `date +"%T" `
mongo mongodb://madmin:madmin@${MONGODB1}:27017 < /scripts/rs-init.js
