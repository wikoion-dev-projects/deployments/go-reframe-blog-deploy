# go-reframe-blog-deploy

Deployments for fullstack blog:
- Reframe-blog is a PWA written in Clojurescript, using Reagent and Re-frame. This is the fontend: https://gitlab.com/wikoion-dev-projects/clojure-projects/reframe-blog
- Golang-blog is an API written in Golang, using Mongodb as datastore, elasticsearch for search and monstache to sync Mongo->Elastic: https://gitlab.com/wikoion-dev-projects/go-projects/golang-blog

## How to Use
Initial release is just docker compose, will be adding kubernetes and swarm deployments later.
### Production deployment:
You'll have to edit the docker-compose file and replace any references to fossg.news with your domain name.
This deployment uses cloudflare for dns challenge. You will need different secrets/environment variables for different providers.
```
git clone
cd go-reframe-blog-deploy/docker/compose

echo -n "YOUR-COULDFLARE-EMAIL" > ../secrets/CF_API_EMAIL.secret
ehco -n "YOUR-CLOUDFLARE-GLOBAL-APIKEY" > ../secrets/CF_API_KEY.secret 

echo -n "YOUR-GOLANG-BLOG-SECRET" > ../secrets/GO_API_KEY.secret

docker-compose up -d
```
You can then reach the app via traefik e.g. https://cmj.fossg.news

### Dev deployment
This is mainly for testing local/feature branch changes to either golang-blog or reframe-blog, any push to master will automatically build a new container, so can then use the production deployment.
```
git clone https://gitlab.com/wikoion-dev-projects/clojure-projects/reframe-blog.git
cd reframe-blog
docker build -t reframe-blog:testing .
cd ..

git clone https://gitlab.com/wikoion-dev-projects/go-projects/golang-blog.git
cd golang-blog
docker build -t golang-blog:testing .
cd ..

git clone
cd go-reframe-blog-deploy/docker/compose
docker-compose -f docker-compose-test.yml up -d
```
The CORS policy in the API will block any requests unless they originate from `https://cmj.fossg.news` or `http://localhost:3001`, if you want to change the port of the frontend then you'll have to modify the CORS acceptedOrigins in `server_helpers.go`
